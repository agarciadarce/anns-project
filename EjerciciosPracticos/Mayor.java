import java.util.Scanner;
public class Mayor{
    public static void main(String[] args){
        int num[] = new int[5];
        int mayor = 0, menor = 0;
        Scanner y = new Scanner(System.in);
        for(int i=0;i<5;i++){
            System.out.println("Introduce un numero: ");
            num[i] = y.nextInt();
        }
        for(int i=0;i<5;i++){
            if(num[i]>mayor){
                mayor = num[i];
            }
        }
        menor = mayor;
        for(int i=0;i<5;i++){
            if(num[i]<menor){
                menor = num[i];
            }
        }
        System.out.println("El mayor es: " + mayor);
        System.out.println("El menor es: " + menor);
    }
}