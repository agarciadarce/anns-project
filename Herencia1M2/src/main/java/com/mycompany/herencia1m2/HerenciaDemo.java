/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.herencia1m2;

import uni.pojo.Empleado;
import uni.pojo.Estudiante;
import uni.pojo.Persona;

/**
 *
 * @author Sistemas36
 */
public class HerenciaDemo {
    public static void main(String[] args){
        /*Empleado e = new Empleado(1, "Marketing Digital", "Pepito", "Perez");
        Estudiante est = new Estudiante("2019-1000U", "Ana", "Perez");
        print(e);
        print(est);*/
        Persona[] personas = {
            new Empleado(1, "Marketing Digital", "Pepito", "Perez"),
            new Estudiante("2019-1000U", "Ana", "Perez")
            
        };
        for(Persona p : personas){
            if(p.compareTo(new Estudiante("", "", "Perez")) == 0){
                print(p);
            }
            
        }
    }
    // Para mandar a imprimir los datos
    /*public static void print(Empleado e){
        System.out.println("Codigo: "+ e.getCodigo());
        System.out.println("Cargo: " + e.getCargo());
        System.out.println("Nombres: " + e.getNombres());
        System.out.println("Apellidos: " + e.getApellidos());
    }*/
    // Polimorfismo
    public static void print(Persona p){
        System.out.println(p.toString());
    }
}
