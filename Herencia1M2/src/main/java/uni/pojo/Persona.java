/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.pojo;

/**
 *
 * @author Sistemas36
 */
// Creando una clase abstracta
public abstract class Persona implements Comparable<Persona>{ // Comparable es una interfaz, permite hacer comparaciones en cualquier objeto
    
    //Creando atributos
    // Una clase hereda de otra clase
    // Una interfaz hereda de otra interfaz
    // Cuando es de clase hereda de una interfaz
    // Cuando es de una interfaz a una clase no existe
   protected String nombres; // Modificador
   protected String apellidos;

   // Constructor
    public Persona(String nombres, String apellidos) {
        this.nombres = nombres;
        this.apellidos = apellidos;
    }
    // Metodos getter y setter
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    } 

    @Override
    public String toString() {
        return "Persona{" + "nombres=" + nombres + ", apellidos=" + apellidos + '}';
    }

    @Override
    public int compareTo(Persona p){
        return this.apellidos.compareToIgnoreCase(p.getApellidos());
    }
    
   
}
