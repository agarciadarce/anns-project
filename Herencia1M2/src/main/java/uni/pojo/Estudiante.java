/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.pojo;

/**
 *
 * @author Sistemas36
 */
public class Estudiante extends Persona{
    private String carnet;
    
    public Estudiante(String carnet, String nombres, String apellidos) {
        super(nombres, apellidos);
        this.carnet = carnet;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    @Override
    public String toString() {
        return "Estudiante{" + 
                "\ncarnet: " + carnet +
                ",\nnombres: " + super.nombres + 
                ",\napellidos: " + super.apellidos +
                '}';
    }
    
}
