// CALCULO DE FACTORIAL UTILIZANDO PARAMETROS
import java.util.Scanner;
public class Factorial{
    public static void main(String args []){
        int fact;
        Scanner scan = new Scanner(System.in);
        System.out.println("Ingrese un numero:");
        fact= calculaFactorial(scan.nextInt());
        System.out.println("El factorial del numero es: "+ fact );
    }
    public static int calculaFactorial(int num){
        int factorial = 1;
        while ( num!=0) {
        factorial=factorial*num;
        num--;
        }
        return factorial;
    }

}