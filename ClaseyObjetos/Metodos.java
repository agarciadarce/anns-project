import java.util.Arrays;
public class Metodos{
    public static void main(String[] args){
        int A[] = {2,4,5,7,8,9,15,18,45};
        
        int matrix[][] = {
            {1,2,-3},
            {4,0,-2}
        };
        int suma = suma(A);
        int sumaTabla = suma(matrix);
        

        printArray(A);
        System.out.println("Suma: " + suma);
        printTable(matrix);
        System.out.println("Suma: " + sumaTabla);
    }
    public static void printArray(int array[]){
        System.out.println(Arrays.toString(array));
    }
    public static void printTable(int matrix[][]){
        for(int i=0;i<matrix.length;i++){
            printArray(matrix[i]);
        }
    }
    // Cabecera del metodo
    public static int suma(int array[]){
        // Definicion del metodo
        int suma = 0;
        for(int a : array){
            suma += a;
        }
        return suma;
    }
    public static int suma(int matrix[][]){
        int suma = 0;
        for(int i=0; i<matrix.length;i++){
            suma += suma(matrix[i]);
        }
        return suma;
    }
    public static int suma(int a, int b){
        return (a+b);
    }
    public static int suma(int a, int b, int c){
        return (a+b+c);
    }
    public static float suma(float a, float b){
        return (a+b);
    }
}