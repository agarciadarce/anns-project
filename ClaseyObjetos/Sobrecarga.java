/*Realizar un programa en JAVA que implemente métodos sobrecargados
para calcular el factorial de un número entero. Un método que devuelva el
resultado del factorial del número y otro que imprima el factorial desde 1
hasta el número.
Por ejemplo:
Numero = 5
Primer Método imprime = 120
Segundo Método imprime
Número 1 2 3 4 5
Factorial 1 2 6 24 120*/
import java.util.Scanner;
public class Sobrecarga{
    private Scanner teclado;
    private int num;
    public void inicializar(){
        teclado = new Scanner(System.in);
        System.out.print("Numero: ");
        num = teclado.nextInt();
    }
    public void imprimir(){
        
        System.out.println("Numero: " + num);
    }
    
    public static int fact(int num){
        int f = 1;
        while(num!=0){
            f=f*num;
            num--;
        }
        return f;
        System.out.println(fact);
    }
    public void  sMetodo(){
        for(int i=0;i<num;i++){
            System.out.print(i);
        }
    }

    public static void main(String[] args) {
        N numero1;
        numero1 = new N();
        numero1.inicializar();
        numero1.fact();
        numero1.sMetodo();
    }

}