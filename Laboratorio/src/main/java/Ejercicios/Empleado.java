/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;
import java.util.Scanner;

/**
 *
 * @author Sistemas36
 */
public class Empleado extends Persona{
    Scanner c = new Scanner(System.in);
    private double SalarioBasico;
    public Empleado(){
        super();
    }

    public Empleado(String nombre, int edad, double SalarioBasico){
        super(nombre, edad);
        this.SalarioBasico = SalarioBasico;
    }
    public double getSalarioBasico(){
        return this.SalarioBasico;
    }
    public void setSalarioBasico(double SalarioBasico){
        this.SalarioBasico = SalarioBasico;
    }
    public String toString() {
        return super.toString() + "Salario: " + getSalarioBasico();
    }
}
