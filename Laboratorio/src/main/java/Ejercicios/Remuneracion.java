/* Antiüedad 10 años = 15%*/
public class Remuneracion extends Persona {
    public Remuneracion(String nombre, int edad, double antiguedad, double INSS, +
    double IR, double INATEC, double vac, double treceavo){
        super(nombre, edad);
        this.antiguedad = antiguedad;
        this.inss = inss;
        this.ir = ir;
        this.inatec = inatec;
        this.vac = vac;
        this.treceavo = treceavo;
    }
    public double getAntiguedad(){
        return this.antiguedad;
    }
    public void setAntiguedad(double antiguedad){
        this.antiguedad = antiguedad;
    }
    public double getInss(){
        return this.inss;
    }
    public void setInss(double inss){
        this.inss = inss;
    }
    public double getIr(){
        return this.ir;
    }
    public void setIr(double ir){
        this.ir = ir;
    }
    public double getInatec(){
        return this.inatec;
    }
    public void setInatec(double inatec){
        this.inatec;
    }
    public double getVac(){
        return this.vac;
    }
    public void setVac(double vac){
        this.vac;
    }
    public double getTreceavo(){
        return this.treceavo;
    }
    public void setTreceavo(double treceavo){
        this.treceavo;
    }
}
