public class Implemento {
    public static void main(String[] args){
        Persona[] datos = {
            new Persona("Ana", 23);
            new Empleado("Ernesto", 18, 5000.67);
        };
        for(Persona p:datos) {
            System.out.println(p.toString());
        }
        
    }
}