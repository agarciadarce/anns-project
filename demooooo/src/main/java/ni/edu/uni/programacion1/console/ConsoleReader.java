/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import ni.edu.uni.programacion1.util.Validator;

/**
 *
 * @author Sistemas36
 */
public class ConsoleReader {
    public static int readInt(BufferedReader reader, String message) throws IOException{
        String text = "";
        boolean flag;
        do{
            System.out.println("message");
            text = reader.readLine();
            flag = Validator.isInt(text);
        }while(!flag);
        return Integer.parseInt(text);
    }
    
    public static String readLetter(BufferedReader reader, String message) throws IOException{
        String text = "";
        boolean flag;
        do{
            System.out.println("message");
            text = reader.readLine();
            flag = Validator.isLetter(text);
        }while(!flag);
        return text;
    }
    
    public static String readCedula(BufferedReader reader, String message) throws IOException{
        String text = "";
        boolean flag;
        do{
            System.out.print("message");
            text = reader.readLine();
            flag = Validator.isCedula(text);
        }while(!flag);
        return text;
    }
     public static String readTelefono(BufferedReader reader, String message) throws IOException{
        String text = "";
        boolean flag;
        do{
            System.out.print("message");
            text = reader.readLine();
            flag = Validator.isPhoneNumber(text);
        }while(!flag);
        return text;
    }
     public static String readString(BufferedReader reader, String message) throws IOException{
         System.out.print(message);
          return reader.readLine();
     }

    
}
