/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.enums;

/**
 *
 * @author Sistemas36
 */
public enum NivelAcademico {
    PRIMARIA,
    TECNICO_MEDIO,
    BACHILLER,
    TECNICO_SUPERIOR,
    LICENCIADO,
    INGENIERO,
    POSTGRADO,
    ESPECIALISTA,
    MAESTRIA,
    DOCTORADO,
    POSTDOCTORADO

}
