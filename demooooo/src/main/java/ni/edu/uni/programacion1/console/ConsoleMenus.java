/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.console;

/**
 *
 * @author Sistemas36
 */
public class ConsoleMenus {
    public static void mainMenu(){
        System.out.println("Bienvenudo a su aplicacion");
        System.out.println("Seleccione una opcion");
        System.out.println("1. Gestion de Empleados");
        System.out.println("2. Reportes de Empleados");
        System.out.println("3. Salir");
        
    }
    public static void gestionSubMenu(){
        System.out.println("1. Agregar");
        System.out.println("2. Editar");
        System.out.println("3. Eliminar");
        System.out.println("4. Regresar");
    }
    public static void reportesSubMenu(){
        System.out.println("1. Visualizar todos");
        System.out.println("2. Visualizar por codigo");
        System.out.println("3. Visualizar por sexo");
        System.out.println("4. Visualizar por Municipio");
        System.out.println("5. Visualizar por nombre");
        System.out.println("6. Visualizar por apellido");
        System.out.println("7. Visualizar por cedula");
        System.out.println("8. Regresar");
    }
}
