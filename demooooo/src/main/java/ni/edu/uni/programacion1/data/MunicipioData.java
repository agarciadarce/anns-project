/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import java.util.Arrays;
import ni.edu.uni.programacion1.pojo.Departamento;
import ni.edu.uni.programacion1.pojo.Municipio;
import ni.edu.uni.programacion1.pojo.Empleado;


/**
 *
 * @author Sistemas36
 */
public class MunicipioData {
    private Municipio municipios[];
    private DepartamentoData dData;

    public MunicipioData() {
        dData = new DepartamentoData();
        populateMunicipios();
    }

    private void populateMunicipios() {
        municipios = new Municipio[]{
            new Municipio(1, "Boaco", dData.getDepartamentoById(1)),
            new Municipio(2, "Camoapa", dData.getDepartamentoById(1)),
            new Municipio(3, "San Lorenzo", dData.getDepartamentoById(1)),
            new Municipio(4, "San Jose de los Remates", dData.getDepartamentoById(1)),
            new Municipio(5, "Santa Lucia", dData.getDepartamentoById(1)),
            new Municipio(6, "Teustepe", dData.getDepartamentoById(1)),
            new Municipio(7, "Diriamba", dData.getDepartamentoById(2)),
            new Municipio(8, "Dolores", dData.getDepartamentoById(2)),
            new Municipio(9, "El Rosario", dData.getDepartamentoById(2)),
            new Municipio(10, "Jinotepe", dData.getDepartamentoById(2)),
            new Municipio(11, "La conquista", dData.getDepartamentoById(2)),
            new Municipio(12, "La Paz de Oriente", dData.getDepartamentoById(2)),
            new Municipio(13, "San Marcos", dData.getDepartamentoById(2)),
            new Municipio(14, "Santa Teresa", dData.getDepartamentoById(2)),
            new Municipio(15, "Chichigalpa", dData.getDepartamentoById(3)),
            new Municipio(16, "Chinandega", dData.getDepartamentoById(3)),
            new Municipio(17, "Cinco Pinos", dData.getDepartamentoById(3)),
            new Municipio(18, "Corinto", dData.getDepartamentoById(3)),
            new Municipio(19, "El Realejo", dData.getDepartamentoById(3)),
            new Municipio(20, "El viejo", dData.getDepartamentoById(3)),
            new Municipio(21, "Posoltega", dData.getDepartamentoById(3)),
            new Municipio(22, "San Francisco del Norte", dData.getDepartamentoById(3)),
            new Municipio(23, "San Pedro del Norte", dData.getDepartamentoById(3)),
            new Municipio(24, "Santo Tomás del Norte", dData.getDepartamentoById(3)),
            new Municipio(25, "Somotillo", dData.getDepartamentoById(3)),
            new Municipio(26, "Puerto morazán", dData.getDepartamentoById(3)),
            new Municipio(27, "Villa Nueva", dData.getDepartamentoById(3)),
            new Municipio(28, "Acoyapa", dData.getDepartamentoById(4)),
            new Municipio(29, "Comalapa", dData.getDepartamentoById(4)),
            new Municipio(30, "San Francisco de cuapa", dData.getDepartamentoById(4)),
            new Municipio(31, "El Coral", dData.getDepartamentoById(4)),
            new Municipio(32, "Juigalpa", dData.getDepartamentoById(4)),
            new Municipio(33, "La Libertad", dData.getDepartamentoById(4)),
            new Municipio(34, "San pedro de Lóvago", dData.getDepartamentoById(4)),
            new Municipio(35, "Santo Domingo", dData.getDepartamentoById(4)),
            new Municipio(36, "Santo Tomás", dData.getDepartamentoById(4)),
            new Municipio(37, "Villa Sandino", dData.getDepartamentoById(4)),
            new Municipio(38, "Puerto Cabezas", dData.getDepartamentoById(5)),
            new Municipio(39, "Bonanzas", dData.getDepartamentoById(5)),
            new Municipio(40, "Mulukuku", dData.getDepartamentoById(5)),
            new Municipio(41, "Prizapolka", dData.getDepartamentoById(5)),
            new Municipio(42, "Rosita", dData.getDepartamentoById(5)),
            new Municipio(43, "siuna", dData.getDepartamentoById(5)),
            new Municipio(44, "Waslala", dData.getDepartamentoById(5)),
            new Municipio(45, "Waspán", dData.getDepartamentoById(5)),
            new Municipio(46, "Bluefields", dData.getDepartamentoById(6)),
            new Municipio(47, "Desembocadura del Rio Grande", dData.getDepartamentoById(6)),
            new Municipio(48, "El Ayote", dData.getDepartamentoById(6)),
            new Municipio(49, "El Rama", dData.getDepartamentoById(6)),
            new Municipio(50, "El Tortuguero", dData.getDepartamentoById(6)),
            new Municipio(51, "Islas del Maíz", dData.getDepartamentoById(6)),
            new Municipio(52, "Kukra Hill", dData.getDepartamentoById(6)),
            new Municipio(53, "La cruz del Rio Grande", dData.getDepartamentoById(6)),
            new Municipio(54, "Laguna de perlas", dData.getDepartamentoById(6)),
            new Municipio(55, "Muelle de los Bueyes", dData.getDepartamentoById(6)),
            new Municipio(56, "Nueva Guinea", dData.getDepartamentoById(6)),
            new Municipio(57, "Paiwas", dData.getDepartamentoById(6)),
            new Municipio(58, "Condega", dData.getDepartamentoById(7)),
            new Municipio(59, "Estelí", dData.getDepartamentoById(7)),
            new Municipio(60, "La trinidad", dData.getDepartamentoById(7)),
            new Municipio(61, "Pueblo Nuevo", dData.getDepartamentoById(7)),
            new Municipio(62, "San Juan de limay", dData.getDepartamentoById(7)),
            new Municipio(63, "San Nicolas", dData.getDepartamentoById(7)),
            new Municipio(64, "Diriá", dData.getDepartamentoById(8)),
            new Municipio(65, "Diriomo", dData.getDepartamentoById(8)),
            new Municipio(66, "Granada", dData.getDepartamentoById(8)),
            new Municipio(67, "Nandaime", dData.getDepartamentoById(8)),
            new Municipio(68, "El cuá", dData.getDepartamentoById(9)),
            new Municipio(69, "Jinotega", dData.getDepartamentoById(9)),
            new Municipio(70, "La concordia", dData.getDepartamentoById(9)),
            new Municipio(71, "San José de Bocay", dData.getDepartamentoById(9)),
            new Municipio(72, "San Rafael del Norte", dData.getDepartamentoById(9)),
            new Municipio(73, "San Sebastian de yalí", dData.getDepartamentoById(9)),
            new Municipio(74, "Santa María de Pantasma", dData.getDepartamentoById(9)),
            new Municipio(75, "Wiwilí de jinotega", dData.getDepartamentoById(9)),
            new Municipio(76, "Achuapa", dData.getDepartamentoById(10)),
            new Municipio(77, "El Jicaral", dData.getDepartamentoById(10)),
            new Municipio(78, "El sauce", dData.getDepartamentoById(10)),
            new Municipio(79, "La paz Centro", dData.getDepartamentoById(10)),
            new Municipio(80, "Larreynaga", dData.getDepartamentoById(10)),
            new Municipio(81, "León", dData.getDepartamentoById(10)),
            new Municipio(82, "Nagarote", dData.getDepartamentoById(10)),
            new Municipio(83, "Quezalguaque", dData.getDepartamentoById(10)),
            new Municipio(84, "Santa Rosa del Peñon", dData.getDepartamentoById(10)),
            new Municipio(85, "Telica", dData.getDepartamentoById(10)),
            new Municipio(86, "Las sabanas", dData.getDepartamentoById(11)),
            new Municipio(87, "Palacaguina", dData.getDepartamentoById(11)),
            new Municipio(88,"San José de Cusmapa", dData.getDepartamentoById(11)),
            new Municipio(89, "San Juan del Río Coco", dData.getDepartamentoById(11)),
            new Municipio(90, "San Lucas", dData.getDepartamentoById(11)),
            new Municipio(91, "Somoto", dData.getDepartamentoById(11)),
            new Municipio(92, "Telpaneca", dData.getDepartamentoById(11)),
            new Municipio(93, "Totogalpa", dData.getDepartamentoById(11)),
            new Municipio(94, "Yalaguina", dData.getDepartamentoById(11)),
            new Municipio(95, "Cuidad Sandino", dData.getDepartamentoById(12)),
            new Municipio(96, "El crucero", dData.getDepartamentoById(12)),
            new Municipio(97, "Managua", dData.getDepartamentoById(12)),
            new Municipio(98, "Mateare", dData.getDepartamentoById(12)),
            new Municipio(99, "San Francisco Libre", dData.getDepartamentoById(12)),
            new Municipio(100, "San Rafael del Sur", dData.getDepartamentoById(12)),
            new Municipio(101, "Ticuantepe", dData.getDepartamentoById(12)),
            new Municipio(102, "Tipitapa", dData.getDepartamentoById(12)),
            new Municipio(103, "Villa del Carmen", dData.getDepartamentoById(12)),
            new Municipio(104, "Catarina", dData.getDepartamentoById(13)),
            new Municipio(105, "La Concepcion", dData.getDepartamentoById(13)),
            new Municipio(106, "Masatepe", dData.getDepartamentoById(13)),
            new Municipio(107, "Masaya", dData.getDepartamentoById(13)),
            new Municipio(108, "Nandasmo", dData.getDepartamentoById(13)),
            new Municipio(109, "Nindirí", dData.getDepartamentoById(13)),
            new Municipio(110, "Niquinohomo", dData.getDepartamentoById(13)),
            new Municipio(111, "San Juan de oriente", dData.getDepartamentoById(13)),
            new Municipio(112, "Tisma", dData.getDepartamentoById(13)),
            new Municipio(113, "Cuidad Darío", dData.getDepartamentoById(14)),
            new Municipio(114, "El Tuma_ La Dalia",dData.getDepartamentoById(14)),           
            new Municipio(115, "Esquipulas", dData.getDepartamentoById(14)),
            new Municipio(116, "Matagalpa", dData.getDepartamentoById(14)),
            new Municipio(117, "Matiguás", dData.getDepartamentoById(14)),
            new Municipio(118, "Muy Muy", dData.getDepartamentoById(14)),
            new Municipio(119, "Rancho Grande", dData.getDepartamentoById(14)),
            new Municipio(120, "Río Blanco", dData.getDepartamentoById(14)),
            new Municipio(121, "San Dionisio", dData.getDepartamentoById(14)),
            new Municipio(122, "San Isidro", dData.getDepartamentoById(14)),
            new Municipio(123, "San Ramón", dData.getDepartamentoById(14)),
            new Municipio(124, "Sébaco", dData.getDepartamentoById(14)),
            new Municipio(125, "Terrabona", dData.getDepartamentoById(14)),
            new Municipio(126, "Cuidad Antigua", dData.getDepartamentoById(15)),
            new Municipio(127, "Dipilto", dData.getDepartamentoById(15)),
            new Municipio(128, "El Jícaro", dData.getDepartamentoById(15)),
            new Municipio(129, "Guiguilí", dData.getDepartamentoById(15)),
            new Municipio(130, "Jalapa", dData.getDepartamentoById(15)),
            new Municipio(131, "Macuelizo", dData.getDepartamentoById(15)),
            new Municipio(132, "Mozonte", dData.getDepartamentoById(15)),
            new Municipio(133, "Murra", dData.getDepartamentoById(15)),
            new Municipio(134, "Ocotal", dData.getDepartamentoById(15)),
            new Municipio(135, "Quilalí", dData.getDepartamentoById(15)),
            new Municipio(136, "San Fernando", dData.getDepartamentoById(15)),
            new Municipio(137, "Santa María", dData.getDepartamentoById(15)),
            new Municipio(138, "El Almendro", dData.getDepartamentoById(16)),
            new Municipio(139, "El Catillo", dData.getDepartamentoById(16)),
            new Municipio(140, "Morrito", dData.getDepartamentoById(16)),
            new Municipio(141, "San Carlos", dData.getDepartamentoById(16)),
            new Municipio(142, "San Juan del Norte", dData.getDepartamentoById(16)),
            new Municipio(143, "San Miguelito", dData.getDepartamentoById(16)),
            new Municipio(144, "Altagracia", dData.getDepartamentoById(17)),
            new Municipio(145, "Belén", dData.getDepartamentoById(17)),
            new Municipio(146, "Buenos Aires", dData.getDepartamentoById(17)),
            new Municipio(147, "Cárdenas", dData.getDepartamentoById(17)),
            new Municipio(148, "Moyogalpa", dData.getDepartamentoById(17)),
            new Municipio(149, "Potosí", dData.getDepartamentoById(17)),
            new Municipio(150, "Rivas", dData.getDepartamentoById(17)),
            new Municipio(151, "San Jorge", dData.getDepartamentoById(17)),
            new Municipio(152, "San Juan del Sur", dData.getDepartamentoById(17)),
        };
    }
    
    private Municipio[] getMunicipios(){
        return this.municipios;
    }
    
    public Municipio getMunicipioById(int id){
        return (id <= 0) ? null : 
               (id > municipios.length) ? null : municipios[id - 1];
    }
    
    public Municipio[] getMunicipioByDepartamento(Departamento d){
        Municipio municipiosByDepartamento[] = null;
        for(Municipio m : this.municipios){
            if(m.getDepartamento().getCodigo() == d.getCodigo()){
                municipiosByDepartamento = 
                        addMunicipio(municipiosByDepartamento, m);
            }
        }
        return municipiosByDepartamento;
    }
    public Municipio[] getVisualizarMunicipio(Municipio d){
        Municipio visualizarMunicipio[] = municipios;
        return municipios;
        
    }
    private Municipio[] addMunicipio(Municipio[] municipioCopy, Municipio m){
        if(municipioCopy == null){
            municipioCopy = new Municipio[1];
            municipioCopy[municipioCopy.length - 1] = m;
        }
        
        municipioCopy = Arrays.copyOf(municipioCopy, municipioCopy.length + 1);
        municipioCopy[municipioCopy.length - 1] = m;
        
        return municipioCopy;
    }
    

}
