/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author TSIS-08
 */
public class Empleado {
private int id;
private String nombres;
private String apellidos;
private float salario;

public Empleado(int id, String nombre, String apellido, float salario) {
this.id = id;
this.nombres = nombre;
this.apellidos = apellido;
this.salario = salario;
}

public int getId() {
return id;
}

public void setId(int id) {
this.id = id;
}

public String getNombre() {
return nombres;
}

public void setNombre(String nombre) {
this.nombres = nombre;
}

public String getApellido() {
return apellidos;
}

public void setApellido(String apellido) {
this.apellidos = apellido;
}

public float getSalario() {
return salario;
}

public void setSalario(float salario) {
this.salario = salario;
}

@Override
public String toString() {
return "Empleado{" + "id=" + id + ", nombre=" + nombres + ", apellido=" + apellidos + ", salario=" + salario + '}';
}
}
