public class Ejercicio2{
    public static void main (String[] args){
        int N = 8;
        double A = 0.5;
        char C = 'h';
        
        System.out.println("***Valor de cada variable***");
        System.out.println("N: " + N);
        System.out.println("A: " + A);
        System.out.println("C: " + C);

        System.out.println("*********");
        System.out.println("Suma: " + (N+A));
        System.out.println("Diferencia: " + (A-N));
        System.out.println("Valor correspondiente de C: " + C);
    }
}